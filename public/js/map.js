var map = L.map('main_map').setView([-34.6012424, -58.3861497], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: 'json',
  //  url: '/api/bicicletas',
    url: '/bicis_map',
    success: function(result){
        console.log(result)
        result.bicicletas.forEach((bici) => {
            L.marker(bici.ubicacion).addTo(map).bindPopup("<b>Codigo: "+bici.code+"</b><br />"+"<b>Modelo: "+bici.modelo+"</b><br />"+"Color: "+bici.color+"<br />Ubicación: "+bici.ubicacion).openPopup();
        })
    }
})