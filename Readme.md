## Curso: Desarrollo del lado servidor: NodeJS, Express y MongoDB

- Utilizo PUG como template engine.

- No olvidar realizar npm install para cargar los módulos.

- Para correr la aplicación:
    iniciar el servidor de mongo con: mongod
    configurado para puerto 3000: http://localhost:3000/
    
- Sitio Online Heroku: https://red-bicicleta-app.herokuapp.com/

- Crear usuario o entrar con: 
                                usuario: b@yopmail password: b

# MIBICI

Red de bicicletas en mi ciudad

## Instalación

Usa el siguiente comando para descargar las dependencias del proyecto.

```bash
npm install
```

## Crear un archivo de variables de ambiente .env en la raiz a efectos del funcionamiento local de la aplicación.

```bash
NODE_ENV='development'
MONGO_URI='mongodb://localhost/red_bicicletas'
ETHEREAL_USER='paul.west68@ethereal.email'
ETHEREAL_PWD='wpVxW7wSMRhb6E153s'
SENDING_EMAIL_ADDRESS='paul.west68@ethereal.email'
APP_BASE_URL='http://localhost:3000'
GOOGLE_CLIENT_ID=''
GOOGLE_CLIENT_SECRET=''
FACEBOOK_ID=''
FACEBOOK_SECRET=''
```

## Usa este comando para ejecutar el proyecto

```bash
npm run devstart
```

## Usa este comando para probar todos los test con jasmine

```bash
npm test
```

## Estas son las url para la API Biciletas

| Acción | URL |
| ------ | ------ |
| Listar(Get) | [http://localhost:3000/api/bicicletas] |
| Crear(Post) | [http://localhost:3000/api/bicicletas/create]|
| Eliminar(Delete) | [http://localhost:3000/api/bicicletas/delete/id]|
| Actualizar(Put) | [http://localhost:3000/api/bicicletas/update/id]|

## Estas son las url para la API Usuarios

| Acción | URL |
| ------ | ------ |
| Listar(Get) | [http://localhost:3000/api/usuarios] |
| Crear reserva(Post) | [http://localhost:3000/api/usuarios/reservar]|
| Crear Usuario(Post) | [http://localhost:3000/api/usuarios/create]|

## Estas son las url para la API de atenticación de Usuarios

| Acción | URL |
| ------ | ------ |
| Autenticarse(Post) | [http://localhost:3000/api/auth/authenticate] |
| Reset password(Post) | [http://localhost:3000/api/auth/forgotpassword]|


## License
[MIT](https://choosealicense.com/licenses/mit/)

Martín Solá - martinsola119@gmail.com